Template.form.events({
    'submit form': function (event) {
        event.preventDefault();

        var imageFile = event.currentTarget.children[0].files[0];

        Collections.Images.insert(imageFile, function (error, fileObject) {
            if (error) {

            } else {
                //submit post data to database
                //Author, Message, Created By Data, ImageID
                //fileObject.id
                $('.gird').masonry('reloadItems');
            }
        });
    }
});